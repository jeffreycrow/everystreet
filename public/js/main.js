const MOBILE_MAX_WIDTH = 430;

function getPolylineData(callback) {
  var request = new XMLHttpRequest();
  var polylineURL = window.location.href + "/data/polylines.json"
  request.open('GET', polylineURL);
  request.responseType = 'json';
  request.send();

  request.onload = function() {
    var polylines = request.response;
    callback(request.response);
  }
}

function getSinglePolyline(polyline) {
  var points = google.maps.geometry.encoding.decodePath(polyline.polyline);
  var polylineObj = new google.maps.Polyline({
      path: points,
      strokeColor: '#95cafa',
      strokeOpacity: 1.0,
      strokeWeight: 5
    });
  return polylineObj;
}

function drawPolylines(map, polylines) {
  for(var i=0;i<polylines.length;i++) {
    var polylineObj = getSinglePolyline(polylines[i]);
    polylines[i].polylineObj = polylineObj
    polylineObj.setMap(map);
  }
}

function initMap(isRideMode) {

  var isTouchDevice = false;
  var isMobile = false;
  var roadLabels = "off";

  if(isRideMode) {
    roadLabels = "on"
  }

  // Create a new StyledMapType object, passing it an array of styles,
  // and the name to be displayed on the map type control.
  var styledMapType = new google.maps.StyledMapType(
    [
      {
        "featureType": "administrative",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "administrative.country",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "administrative.province",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#ffffff"
          },
          {
            "visibility": "on"
          }
        ]
      },
      {
        "featureType": "landscape.natural",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "poi",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road",
        "stylers": [
          {
            "color": "#cccccc"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
          {
            "color": "#000"
          },
          {
            "visibility": "on"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "labels.text",
        "stylers": [
          {
            "visibility": roadLabels
          },
          {
            "weight": 0.5
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#ffffff"
          },
          {
            "weight": 8
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#484848"
          },
          {
            "weight": 2.5
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels.text",
        "stylers": [
          {
            "visibility": roadLabels
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#ffffff"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#484848"
          },
          {
            "weight": 2.5
          }
        ]
      },
      {
        "featureType": "transit",
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "transit.line",
        "elementType": "labels.text",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "transit.station.airport",
        "elementType": "geometry",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "transit.station.airport",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#FFFFFF"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      }
    ]
  );

  if(('ontouchstart' in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0)) {
    isTouchDevice = true;
  }

  // set mobile scroll down click handler
  // set zoom
  var vw = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  if(vw <= MOBILE_MAX_WIDTH) {
    isMobile = true;
    // set mobile scroll down click handler
    // set zoom
  }

  var map = new google.maps.Map(document.getElementById('map-container'), {
    center: {lat: 30.3512, lng: -97.7201},
    zoom: isMobile ? 13 : 13,
    minZoom: 12,
    maxZoom: isRideMode ? 17 : 15,
    disableDefaultUI: true,
    zoomControl: !isTouchDevice,
    options: {
      gestureHandling: 'greedy'
    }
  });

  map.mapTypes.set('styled_map', styledMapType);
  map.setMapTypeId('styled_map');

  return map;
}

function setLastRideDate(polylines) {
  var times = polylines.map(function(i) {
    // have to replace dashes with slashes because Safari is a pain
    return new Date(i.time.replace(/-/g, '/'));
  });

  var max = times.reduce(function (a, b) { return a > b ? a : b; });
  var intlDateOptions = { month: 'long'};
  var prettyDateMonth = new Intl.DateTimeFormat('en-US', intlDateOptions).format(max);
  var prettyDate = prettyDateMonth+" "+max.getUTCDate()+", "+max.getFullYear()

  var dateElem = document.getElementById('lastRide');
  dateElem.innerText = prettyDate;
}

function handleDebugClick(e) {
  var clickId = e.target.getAttribute('data-id');
  for(var i=0;i<window.polylines.length;i++) {
    if(window.polylines[i].id == clickId) {
      window.polylines[i].polylineObj.setVisible(!window.polylines[i].polylineObj.getVisible());
    }
  }
}

function debug() {
  var pane = document.createElement("div");
  pane.setAttribute("id", "debugPane");
  var h2 = document.createElement("h2");
  h2.innerText="Polylines";

  var list = document.createElement("ul");
  list.setAttribute("id", "polylineList");

  for(var i=0;i<window.polylines.length;i++) {
    var pl = document.createElement("li");

    var checkbox = document.createElement("input");
    checkbox.setAttribute("type", "checkbox");
    checkbox.setAttribute("id", "polylineToggle["+i+"]")
    checkbox.setAttribute("data-id", window.polylines[i].id)
    checkbox.checked = window.polylines[i].visible;
    checkbox.onclick = handleDebugClick;

    pl.appendChild(checkbox)

    var span = document.createElement("span");
    span.innerText = window.polylines[i].id
    pl.appendChild(span)

    list.appendChild(pl)
  }

  var close = document.createElement("a");
  close.setAttribute("id", "debugClose");
  close.innerText = "close";
  close.setAttribute("href","#")

  pane.appendChild(h2);
  pane.appendChild(list);
  pane.appendChild(close);
  pane.setAttribute("class","debug-visible");
  var body = document.getElementById('body')
  body.appendChild(pane)
}

function scrollToBottom() {
  window.isScrolledDown = !window.isScrolledDown;
  window.scroll({
    top: document.body.scrollHeight,
    left: 0, 
    behavior: 'smooth'
  });

  var chevron = document.getElementById('scroll-target');
  var chevronUpElement = chevron.getElementsByClassName('chevron-up')[0]
  var chevronDownElement = chevron.getElementsByClassName('chevron-down')[0]

  if(window.isScrolledDown) {
    chevronUpElement.classList.remove('hidden');
    chevronDownElement.classList.add('hidden');
  } else {
    chevronDownElement.classList.remove('hidden');
    chevronUpElement.classList.add('hidden');
  }
}

function initApp() {
  var params = new URLSearchParams(window.location.search);
  var isRideMode = params.has("ride");
  history.replaceState({}, document.title, ".");

  window.mapObj = initMap(isRideMode)

  if(!navigator.geolocation) {
    console.error("Geolocation not supported.")
  } else if(isRideMode) {
    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };

    var myloc = new google.maps.Marker({
        clickable: false,
        icon: new google.maps.MarkerImage('//maps.gstatic.com/mapfiles/mobile/mobileimgs2.png',
                                                        new google.maps.Size(22,22),
                                                        new google.maps.Point(0,18),
                                                        new google.maps.Point(11,11)),
        shadow: null,
        zIndex: 999,
        map: window.mapObj
    });

    id = navigator.geolocation.watchPosition(success, error, options);

    function success(pos) {
      var crd = pos.coords;
      var me = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
      myloc.setPosition(me);

    }

    function error(err) {
      console.warn(`ERROR(${err.code}): ${err.message}`);
    }

  }
  
  getPolylineData(function(polylineData) {
    window.polylines = polylineData
    for(var i=0;i<window.polylines.length;i++) {
      window.polylines[i].visible = true;
    }

    //setLastRideDate(window.polylines)
    drawPolylines(window.mapObj, window.polylines)
  });
}
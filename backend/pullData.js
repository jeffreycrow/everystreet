const Strava = {
  tokenURL: 'https://www.strava.com/api/v3/oauth/token',
  allActivitiesURL: 'https://www.strava.com/api/v3/athlete/activities',
  activityStreamURL: (activityId) => `https://www.strava.com/api/v3/activities/${activityId}/streams`,

  checkRequiredArguments(argv) {
    if(typeof argv.strava_client_id=='undefined') {
      return false
    }
    if(typeof argv.strava_client_secret=='undefined') {
      return false
    }
    return true
  },

  refreshTokens: async function(clientId, clientSecret, currentRefreshToken) {
    console.log("Refreshing tokens...")
    let response = await axios.post(this.tokenURL, {
      client_id: clientId,
      client_secret: clientSecret,
      grant_type: 'refresh_token',
      refresh_token: currentRefreshToken
    })

    return { accessToken: response.data.access_token, refreshToken: response.data.refresh_token }
  },

  getAllActivities: async function(accessToken, after) {
    let response = await axios.get(this.allActivitiesURL, {
      params: {
        after: after
      },
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    }).catch(function (error) {
      console.log("ERR",error)
    })

    let returnArr = []
    for(let i=0;i<response.data.length;i++) {
      let item=response.data[i]
      if(item.type!='Ride') {
        console.log('Skipping activity id #'+item.id+' because it is type '+item.type)
        continue
      }

      returnArr.push({
        id: 'strava_'+item.id,
        strava_id: item.id,
        time: item.start_date,
        polyline: item.map.summary_polyline
      })
    }

    return returnArr
  },

  getActivityLatLng: async function(accessToken, activityId) {
    console.log('Get activity id #'+activityId)
    let response = await axios.get(this.activityStreamURL(activityId), {
      params: {
        keys: 'latlng',
        keys_by_type: true
      },
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    }).catch(function (error) {
      console.log("Error getting activity stream: "+error)
      return false
    })

    let latlng = response.data.find(o => o.type === 'latlng');
    return latlng.data
  },

  getNewRides: async function(clientId, clientSecret, tokenObj, lastRunObj) {
    console.log('Get new rides...')
    let activityArr = await Strava.getAllActivities(tokenObj.accessToken, lastRunObj.lastRideTime)

    for(let i=0;i<activityArr.length;i++) {
      activityArr[i].points = await Strava.getActivityLatLng(tokenObj.accessToken, activityArr[i].strava_id)
    }

    return activityArr
  }
}

function randomNumberFromRange(min, max) {
  return Math.random() * (max - min) + min; 
}

function convertLatLngToArray(points) {
    var returnArr = []
    for(var i=0;i<points.length;i++) {
        returnArr.push([points[i].lat, points[i].lng])
    }
    
    return returnArr
}

function removeNearbyPoints(points, homeLat, homeLng, distanceFromHome) {
    // This function removes any points from the beginning and end of a 
    // set of lat/lng points that are within the distance specified from
    // a home point. The goal is to obscure where that home point actually lies
    // as all trips will start and end there.

    let returnObj = [];

    for(let i=0;i<points.length;i++) {
      if( points[i].lat != 'undefined' ) {
        returnObj.push(points[i])
      }
    }

    var startDistanceSatisfiedFlag = false

    // this loop removes the too close points from the start of the array
    for(var i=0;i<returnObj.length;i++) {
      var distance = getDistance({latitude: homeLat, longitude: homeLng}, {latitude: returnObj[i][0], longitude: returnObj[i][0]})
      if( distance <= distanceFromHome ) {
          continue;
      } else {
          returnObj = returnObj.slice(i)
          break;
      }
    }
    
    // this loop removes the too close points from the end of the array
    for(i=returnObj.length-1;i>=0;i--) {
      distance = getDistance({latitude: homeLat, longitude: homeLng}, {latitude: returnObj[i][0], longitude: returnObj[i][1]})
      if( distance <= distanceFromHome ) {
          continue;
      } else {
          returnObj = returnObj.slice(0,i+1)
          break;
      }
    }
    
    return returnObj

}

function convertPointsToSimplifyFormat(points) {
    var simplifyArr = []
    for(var i=0;i<points.length;i++) {
        simplifyArr.push({x: points[i][0], y: points[i][1]})
    }
    
    return simplifyArr
}

function convertFromSimplifyToPolylineFormat(points) {
    var returnArr = []
    for(var i=0;i<points.length;i++) {
        returnArr.push([points[i].x, points[i].y])
    }
    return returnArr
}

function convertFromLatLngToPolylineFormat(points) {
    var returnArr = []
    for(var i=0;i<points.length;i++) {
        returnArr.push([points[i].latitude, points[i].longitude])
    }
    return returnArr
}

const getSnappedPoints = async (points, apiKey) => {
  let offsetSize = 100;
  let snappedPoints = [];
  let keepGoing = true;
  let offset = 0;
  while (keepGoing) {
    if( offset+offsetSize > points.length - 1 ) {
      endOffset = points.length - 1;
    } else {
      endOffset = offset+offsetSize;
    }

    // If our overall points count is an exact multiple of our offset size we
    // need to run this check or we'll pass an empty set to the function below
    if(offset==endOffset) {
      return snappedPoints
    }

    const offsetPoints = await getSnappedPointsFromOffset(points.slice(offset, endOffset), apiKey)

    await snappedPoints.push.apply(snappedPoints, offsetPoints);
    offset += offsetSize;
    if(points.length - 1 < offset) {
      keepGoing = false;

      return snappedPoints;
    }
  }
}

const getSnappedPointsFromOffset = async (points, apiKey) => {
  const url = 'https://roads.googleapis.com/v1/snapToRoads';
  let pointsStr = '';
  let returnPoints = [];

  for(let i=0;i<points.length;i++) {
    pointsStr += points[i].x + ',' + points[i].y + '|';
  }
  pointsStr = pointsStr.substring(0, pointsStr.length-1)

  const response = await axios.get(url, {
    params: {
      interpolate: true,
      key: apiKey,
      path: pointsStr

    }
  }).catch(function (error) {
      console.log("Error with Google Roads API")
      console.log(error.toJSON())
      return false
  })

  for(let i=0;i<response.data.snappedPoints.length;i++) {
    returnPoints.push(response.data.snappedPoints[i].location);
  }

  return returnPoints;

}

async function getLastRunData(lastRefreshTokenFilename, jobName, gitlabPrivateToken) {
  // Latest CI artifact URL:
  // https://example.com/<namespace>/<project>/-/jobs/artifacts/<ref>/raw/<path_to_file>?job=<job_name>

  let refreshTokenURL = artifactURL(lastRefreshTokenFilename, jobName)
  console.log("Pulling last run data...")
  let currentRefreshToken = undefined
  let response = await axios.get(refreshTokenURL, {
    headers: {
      'PRIVATE-TOKEN': gitlabPrivateToken
    }
  }).catch(function (error) {
    console.log("Error pulling last run data. Using hard coded values.")
    console.log(error.toJSON())
    return {"currentRefreshToken":"6772eb87e9d59212da7f7cd59b3d4f6ad8ac5290","lastRideTime":"2020-11-19T18:45:25Z"}
  })

  return response.data
}

async function processRides(ridesArr, homeLat, homeLng, distanceFromHome, simplifyTolerance, googleApiKey) {
  for(let i=0;i<ridesArr.length;i++) {
    ridesArr[i].points = removeNearbyPoints(ridesArr[i].points, homeLat, homeLng, distanceFromHome)
    let simplifiedPoints = simplify(convertPointsToSimplifyFormat(ridesArr[i].points), simplifyTolerance)
    let snappedPoints = await getSnappedPoints(simplifiedPoints, googleApiKey)
    ridesArr[i].scrubbedPolyline = polylineEncode(convertFromLatLngToPolylineFormat(snappedPoints))
    ridesArr[i].points = convertFromSimplifyToPolylineFormat(simplifiedPoints)
  }

  return ridesArr
}

async function getExistingPolylineFileObj(polylinesFilename, jobName, oldFileURL, gitlabPrivateToken) {
  console.log('Pulling existing polyline file...')
  let fileURL = artifactURL(polylinesFilename, jobName)
  let status = undefined
  let response = await axios.get(fileURL, {
      headers: {
        'PRIVATE-TOKEN': gitlabPrivateToken
      }
    })
    .catch(function (error) {
      console.log("Error pulling existing polyline file.")
      console.log(error.toJSON())
      status = error.response.status
      return false
    })

  if(status==404) {
    console.log('Pulling backup polyline file...')
    // get old file
    response = await axios.get(oldFileURL)
      .catch(function (error) {
        console.log("Error pulling backup polyline file.")
        return false
      })
  }

  return response.data
}

function generatePolylinesObj(ridesArr, existingPolylines) {
  let polylineArr = existingPolylines

  for(let i=0;i<ridesArr.length;i++) {
    polylineArr.push({id: ridesArr[i].id, time: ridesArr[i].time, polyline: ridesArr[i].scrubbedPolyline})
  }

  return polylineArr
}

function writePolylinesFile(polylineArr, polylinesFilePath) {
  console.log('Writing polyline file.')
  fs.writeFile(polylinesFilePath+polylinesFilename, JSON.stringify(polylineArr), (err) => {
    if (err) {
      console.error(err);
      return;
    }
  })
}

function writeLastRunFile(lastRefreshTokenFilename, refreshToken, ridesArr) {
  let newestDate = ridesArr.reduce((a, b) => {
    return new Date(a.time) > new Date(b.time) ? a.time : b.time;
  })

  let writeObj = { currentRefreshToken: refreshToken, lastRideTime: newestDate}
  fs.writeFile(polylinesFilePath+lastRefreshTokenFilename, JSON.stringify(writeObj), (err) => {
    if (err) {
      console.error(err);
      return;
    }
  })
}

function checkRequiredArguments(argv) {
  if(typeof argv.home_lat=='undefined' || typeof argv.home_lng=='undefined') {
    return false
  }
  if(typeof argv.google_api_key=='undefined') {
    return false
  }
  if(typeof argv.gitlab_private_token=='undefined') {
    return false
  }

  return true
}

function artifactURL(fileName, jobName) {
  return `https://gitlab.com/api/v4/projects/jeffreycrow%2Feverystreet/jobs/artifacts/master/raw/data/${fileName}?job=${jobName}`
}

async function main() {
  if(!checkRequiredArguments(argv) && !Strava.checkRequiredArguments(argv)) {
    return false
  }

  let lastRunObj = await getLastRunData(lastRefreshTokenFilename, jobName, argv.gitlab_private_token)
  lastRunObj = {currentRefreshToken: '6772eb87e9d59212da7f7cd59b3d4f6ad8ac5290', lastRideTime: '2020-01-11T19:47:31Z'}
  let tokenObj = await Strava.refreshTokens(argv.strava_client_id, argv.strava_client_secret, lastRunObj.currentRefreshToken)
  let existingRides = await getExistingPolylineFileObj(polylinesFilename, jobName, oldFileURL, argv.gitlab_private_token)
  let newRides = await Strava.getNewRides(argv.strava_client_id, argv.strava_client_secret, tokenObj, lastRunObj)
  let processedRides = await processRides(newRides, argv.home_lat, argv.home_lng, distanceFromHome, simplifyTolerance, argv.google_api_key)
  let polylinesArr = generatePolylinesObj(processedRides, existingRides)
  writePolylinesFile(polylinesArr, polylinesFilePath)
  writeLastRunFile(lastRefreshTokenFilename, tokenObj.refreshToken, newRides)
}

// dependencies
var fs = require("fs")

var argv = require('minimist')(process.argv.slice(2))

var getDistance = require('geolib').getDistance
var polylineEncode = require('@mapbox/polyline').encode
const simplify = require('simplify-js')
const axios = require('axios')

let accessToken = undefined
let refreshToken = undefined
let distanceFromHome = randomNumberFromRange(350, 550)
var polylines = []
var simplifyTolerance = 0.00003
let oldFileURL = 'https://jeffreycrow.gitlab.io/everystreet/polylines_old.json'
let lastRefreshTokenFilename = 'last_run.json'
let polylinesFilename = 'polylines.json'
let polylinesFilePath = './data/'
let jobName = 'pull_data'

main()
